from manimlib import *


class EpicycloidSceneSimple(Scene):
    def construct(self):
       radius1 = 2.4
       radius2 = radius1/3
       self.epy(radius1,radius2)

    def epy(self,r1,r2):
        # Manim circle
        c1 = Circle(radius=r1,color=BLUE)
        # Small circle
        c2 = Circle(radius=r2,color=PURPLE).rotate(PI)
        c2.next_to(c1,RIGHT,buff=0)
        c2.start = c2.copy()
        # Dot
        # .points[0] return the start path coordinate
        # .points[-1] return the end path coordinate
        dot = Dot(c2.get_points()[-1],color=RED)
        # Line
        line = Line(c2.get_center(),dot.get_center()).set_stroke(BLACK,2.5)
        # Path
        path = VMobject(color=RED)
        # Path can't have the same coord twice, so we have to dummy point
        path.set_points_as_corners([dot.get_center(),dot.get_center()+UP*0.001])
        # Path group
        path_group = VGroup(line,dot,path)
        # Alpha, from 0 to 1:
        alpha = ValueTracker(0)
        
        self.play(ShowCreation(line),ShowCreation(c1),ShowCreation(c2),GrowFromCenter(dot))

        # update function of path_group
        def update_group(group):
            l,mob,previus_path = group
            mob.move_to(c2.get_points()[-1])
            previous_path = path.deepcopy()
            previous_path.add_points_as_corners([mob.get_center()])
            path.become(previous_path)
            l.put_start_and_end_on(c2.get_center(),mob.get_center())
            path.become(previous_path)

        # update function of small circle
        def update_c2(c):
            c.become(c.start)
            c.rotate(TAU*alpha.get_value(),about_point=c1.get_center())
            c.rotate(TAU*(r1/r2)*alpha.get_value(),about_point=c.get_center())

        path_group.add_updater(update_group)
        c2.add_updater(update_c2)
        self.add(c2,path_group)
        self.play(
                alpha.set_value,1,
                rate_func=linear,
                run_time=6
                )
        self.wait(2)
        c2.clear_updaters()
        path_group.clear_updaters()
        self.play(FadeOut(VGroup(c1,c2,path_group)))


class EpicycloidSceneSimple_alpha(Scene):
    def construct(self):
       radius1 = 2.4
       radius2 = radius1/3
       self.epy(radius1,radius2)

    def epy(self,r1,r2):
        # Manim circle
        c1 = Circle(radius=r1,color=BLUE)
        # Small circle
        c2 = Circle(radius=r2,color=PURPLE).rotate(PI)
        c2.next_to(c1,RIGHT,buff=0)
        c2.start = c2.copy()
        # Dot
        dot = Dot(c2.get_points()[0],color=YELLOW)
        # Line
        line = Line(c2.get_center(),dot.get_center()).set_stroke(BLACK,2.5)
        # Path
        path = VMobject(color=RED)
        path.set_points_as_corners([dot.get_center(),dot.get_center()+UP*0.001])
        # Path group
        path_group = VGroup(line,dot,path)
        
        self.play(ShowCreation(line),ShowCreation(c1),ShowCreation(c2),GrowFromCenter(dot))

        # update function of path_group
        def update_group(group):
            l,mob,previus_path = group
            mob.move_to(c2.get_points()[-1])
            previous_path = path.deepcopy()
            previous_path.add_points_as_corners([mob.get_center()])
            path.become(previous_path)
            l.put_start_and_end_on(c2.get_center(),mob.get_center())
            path.become(previous_path)

        # update function of small circle
        def update_c2(c,alpha):
            c.become(c.start)
            c.rotate(TAU*alpha,about_point=c1.get_center())
            c.rotate(TAU*(r1/r2)*alpha,about_point=c.get_center())

        path_group.add_updater(update_group)
        self.add(path_group)
        
        self.play(
                UpdateFromAlphaFunc(c2,update_c2,rate_func=linear,run_time=6)
                )
        self.wait()
